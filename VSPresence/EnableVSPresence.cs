﻿using EnvDTE;
using EnvDTE80;
using Microsoft.VisualStudio.Shell;
using System;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Timers;
using Task = System.Threading.Tasks.Task;

namespace VSRichPresence {

    internal sealed class EnableVSPresence {
        private static readonly string[] Supported = { "asm", "c", "cpp", "cs", "css", "h", "html", "java", "js", "json", "lua", "php", "py", "rb", "txt", "xml" };

        public static RichClass RichClass = new RichClass();
        public static bool Shutdown;
        private static bool _initialize;

        private static DiscordRpc.RichPresence _presence;
        private static int _callbackCalls;
        private static DiscordRpc.EventHandlers _handlers;


        private const int CommandId = 0x0100;

        private static readonly Guid CommandSet = new Guid("9cead51b-d641-4969-a680-992f7edef830");

        private readonly Package _package;

        private EnableVSPresence(Package package) {
            _package = package ?? throw new ArgumentNullException(nameof(package));

            if (!(ServiceProvider.GetService(typeof(IMenuCommandService)) is OleMenuCommandService commandService)) return;
            CommandID menuCommandId = new CommandID(CommandSet, CommandId);
            MenuCommand menuItem = new MenuCommand(MenuItemCallback, menuCommandId);
            commandService.AddCommand(menuItem);
        }

        private IServiceProvider ServiceProvider => _package;

        public static void Initialize(Package package) {
            new EnableVSPresence(package);
        }

        public static void StartUp() {
            Console.WriteLine("Discord: init");
            _callbackCalls = 0;

            _handlers = new DiscordRpc.EventHandlers { ReadyCallback = ReadyCallback };
            _handlers.DisconnectedCallback += DisconnectedCallback;
            _handlers.ErrorCallback += ErrorCallback;

            Shutdown = false;
            _initialize = true;

            DiscordRpc.Initialize("395355431356071936", ref _handlers, true, string.Empty);
            _presence.SmallImageKey = "icon";
            _presence.SmallImageText = "Provided by VSPresence";
            UpdatePresenceVs();
            Update();
        }

        private static async void UpdatePresenceVs() => await Task.Run(() => {
            DTE2 dte2 = Package.GetGlobalService(typeof(DTE)) as DTE2;
            Solution solution = dte2?.Solution;
            try {
                string fileName = Path.GetFileName(dte2?.ActiveDocument?.FullName);
                if (solution != null && solution.IsOpen && !string.IsNullOrEmpty(fileName)) {
                    if (!RichClass.Secretmode) {
                        string[] solutionName = solution.FullName.Split("\\".ToCharArray());
                        _presence.LargeImageKey = GetLanguage(fileName);
                        _presence.LargeImageText = $"Currently working in {GetLanguage(fileName).ToUpper()}";

                        _presence.Details = string.Format("Solution: " + solutionName[solutionName.Length - 1].Replace(".sln", ""));
                        _presence.State = $"File: {fileName}";
                        _presence.Instance = false;
                    }
                    else {
                        _presence.Details = string.Format($"I'm working on something you're not");
                        _presence.State = "allowed to know about. Sorry.";
                        _presence.LargeImageKey = "";
                    }
                }
                else {
                    _presence.Details = "I\'m currently not working on anything.";
                    _presence.State = "";
                    _presence.LargeImageText = "So lazy";
                    _presence.LargeImageKey = "lazy";
                }
            }
            catch (Exception) { return; }
            DiscordRpc.UpdatePresence(ref _presence);
        });

        private static string GetLanguage(string fileName) {
            string ext = Path.GetExtension(fileName)?.Replace(".", "");
            return Supported.Any(s => string.Equals(ext, s, StringComparison.CurrentCultureIgnoreCase)) ? ext : "unknown";
        }


        private static void ReadyCallback() {
            ++_callbackCalls;
            Debug.WriteLine("Discord Client: ready");
        }

        private static void DisconnectedCallback(int errorCode, string message) {
            ++_callbackCalls;
            Debug.WriteLine("Discord Client: disconnect {0}: {1}", errorCode, message);
        }

        private static void ErrorCallback(int errorCode, string message) {
            ++_callbackCalls;
            Debug.WriteLine("Discord Client: error {0}: {1}", errorCode, message);
        }

        private static async void Update() {
            if (!Shutdown) {
                await Task.Run(() => {
                    DiscordRpc.RunCallbacks();
                    Timer timer = new Timer {
                        AutoReset = false,
                        Interval = 1000 * 5
                    };
                    timer.Elapsed += Timer_Elapsed;
                    timer.Start();
                });
            }
        }

        private static void Timer_Elapsed(object sender, ElapsedEventArgs e) {
            if (Shutdown) return;
            UpdatePresenceVs();
            Update();
        }

        private static void MenuItemCallback(object sender, EventArgs e) {
            if (Shutdown || !_initialize) {
                StartUp();
            }
        }
    }
}
